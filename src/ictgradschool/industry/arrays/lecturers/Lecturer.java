package ictgradschool.industry.arrays.lecturers;

public class Lecturer {

    // instance variables
    private String name;
    private int staffId;
    private String[] papers;
    private boolean onLeave;
    
    public Lecturer(String name, int staffId, String[] papers, boolean onLeave) {

        this.name = name;
        this.staffId = staffId;
        this.papers = papers;
        this.onLeave = onLeave;
    }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getStaffId(){
            return staffId;
        }

        public void setStaffId(int staffId){
            this.staffId = staffId;
        }

        public String[] getPapers(){
            return papers;
        }

        public void setPapers(String[] papers){
            this.papers = papers;
        }

        public boolean isOnLeave(){
            return onLeave;
        }

        public void setOnLeave(boolean onLeave){
            this.onLeave = onLeave;
        }

        public String toString() {
            return "id: "+ staffId + " " + name + " is teaching " + papers.length + " papers";
        }

        public boolean teachesMorePapersThan(Lecturer otherLecturer) {

            return this.papers.length > otherLecturer.papers.length;
        }

            // TODO Complete this constructor method

    
    // TODO Insert getName() method here
    
    // TODO Insert setName() method here
    
    // TODO Insert getStaffId() method here
    
    // TODO Insert setStaffId() method here
    
    // TODO Insert getPapers() method here
    
    // TODO Insert setPapers() method here
    
    // TODO Insert isOnLeave() method here
    
    // TODO Insert setOnLeave() method here
    
    // TODO Insert toString() method here
    
    // TODO Insert teachesMorePapersThan() method here

}


