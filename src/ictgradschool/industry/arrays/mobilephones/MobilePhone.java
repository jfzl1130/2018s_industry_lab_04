package ictgradschool.industry.arrays.mobilephones;

public class MobilePhone {

    private String brand;
    private String model;
    private double price;


    // TODO Declare the 3 instance variables:
    // brand
    // model
    // price

    public MobilePhone(String brand, String model, double price) {

        this.brand = brand;
        this.model = model;
        this.price = price;

        // Complete this constructor method
    }

    // TODO Uncomment these methods once the corresponding instance variable has been declared.
    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {

        this.price = price;
    }

    public String toString() {

        return brand + model + "which cost $" + price;
    }

    public boolean isCheaperThan(MobilePhone other) {

        if (this.price < other.price) {
            return true;
        }

        return false;

    }

    public boolean equals(MobilePhone other) {
        if (this.model.equals(other.model) && this.brand.equals(other.brand) && this.price==price) {
            return true;
        }
        return false;
    }


    // TODO Insert getModel() method here
    
    // TODO Insert setModel() method here
    
    // TODO Insert getPrice() method here
    
    // TODO Insert setPrice() method here
    
    // TODO Insert toString() method here
    
    // TODO Insert isCheaperThan() method here
    
    // TODO Insert equals() method here

}


