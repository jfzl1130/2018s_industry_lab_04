package ictgradschool.industry.arrays.printpattern;

public class Pattern {
    private Character symbol;
    private int numberOfCharacters;

    public Pattern(int c, char s) {
        symbol = s;
        numberOfCharacters = c;
    }

    public String toString() {
     String string="";
        for (int i = 0; i <numberOfCharacters ; i++) {
            string+=symbol;
        }
    return string;}

    public int getNumberOfCharacters() {
        return numberOfCharacters;
    }

    public void setNumberOfCharacters(int numberOfCharacters) {
        this.numberOfCharacters = numberOfCharacters;
    }
}